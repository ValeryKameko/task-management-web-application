import {
  src, series, dest,
} from 'gulp';
import merge from 'merge-stream';
import del from 'del';
import eslint from 'gulp-eslint';
import babel from 'gulp-babel';
import nodemon from 'gulp-nodemon';
import sass from 'gulp-sass';

const paths = {
  sass: {
    compile: 'src/styles/**/*.@(sass|scss)',
    include: [
      'src/styles/**/*.@(sass|scss)',
      'node_modules/foundation-sites/scss',
    ],
  },
  css: {
    vendors: [
      {
        from: 'node_modules/foundation-sites/dist/css/foundation.min.css',
        to: 'dist/public/vendor/foundation/css',
      },
      {
        from: 'node_modules/foundation-sites/dist/css/foundation-prototype.min.css',
        to: 'dist/public/vendor/foundation/css',
      },
      {
        from: 'node_modules/@yaireo/tagify/dist/tagify.css',
        to: 'dist/public/vendor/tagify/css',
      },
    ],
    destDir: 'dist/public/css',
  },
  js: {
    vendors: [
      {
        from: 'node_modules/@fortawesome/fontawesome-free/js/all.min.js',
        to: 'dist/public/vendor/fontawesome/js',
      },
      {
        from: 'node_modules/foundation-sites/dist/js/foundation.min.js',
        to: 'dist/public/vendor/foundation/js',
      },
      {
        from: 'node_modules/jquery/dist/jquery.min.js',
        to: 'dist/public/vendor/jquery/js',
      },
      {
        from: 'node_modules/@yaireo/tagify/dist/tagify.min.js',
        to: 'dist/public/vendor/tagify/js',
      },
    ],
    sources: [
      'config/**/*.js',
      'src/**/*.js',
    ],
    all: '**/*.js',
    builtApp: 'dist/src/app.js',
  },
  pug: {
    from: 'src/views/**',
    to: 'dist/src/views',
  },
  srcDir: 'src',
  configDir: 'config',
  distDir: 'dist',
  nodeModulesFiles: 'node_modules/**',
  distFiles: 'dist/**',
};

export function lint() {
  return src([
    paths.js.all,
    `!${paths.nodeModulesFiles}`,
    `!${paths.distFiles}`,
  ], { base: '.' })
    .pipe(eslint())
    .pipe(eslint.format());
}

export function buildJs() {
  return src(paths.js.sources, { base: '.' })
    .pipe(babel())
    .pipe(dest(paths.distDir));
}

export function buildVendorJs() {
  return merge(paths.js.vendors.map(({ from, to }) => (
    src(from)
      .pipe(dest(to)))));
}

export function buildVendorCss() {
  return merge(paths.css.vendors.map(({ from, to }) => (
    src(from)
      .pipe(dest(to)))));
}

export function copyViews() {
  return src([
    paths.pug.from,
  ], { base: '.' })
    .pipe(dest(paths.pug.to));
}

export function buildStyles() {
  return src(paths.sass.compile)
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: paths.sass.include,
    }).on('error', sass.logError))
    .pipe(dest(paths.css.destDir));
}

export function clean() {
  return del(paths.distDir);
}

export const build = series(
  lint,
  buildJs,
  buildStyles,
  buildVendorCss,
  buildVendorJs,
  copyViews,
);

export function devServer() {
  return nodemon({
    script: paths.js.builtApp,
    watch: [paths.srcDir, paths.configDir],
    ext: 'js pug sass scss',
    env: { NODE_ENV: 'development' },
    tasks: ['build'],
  });
}

export const serve = series(
  build,
  devServer,
);

export default serve;
