const config = {
  port: process.env.APP_PORT || 9000,
  db: {
    mongoose_debug: false,
  },
  files: {
    resourceFolder: process.env.RESOURCES_DIR || '/data/resources',
    uploadFolder: process.env.UPLOAD_DIR || '/data/upload',
  },
  developmentLog: false,
};

export default config;
