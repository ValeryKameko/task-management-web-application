const config = {
  host: process.env.APP_HOST || 'localhost',
  db: {
    username: process.env.MONGO_USERNAME || 'task_management_application_web',
    password: process.env.MONGO_PASSWORD || 'task_management_application_password',
    hostname: process.env.MONGO_HOSTNAME || 'mongo',
    port: process.env.MONGO_PORT || '27017',
    schema: process.env.MONGO_SCHEMA || 'task_management_application',
    mongoose_debug: process.env.MONGOOSE_DEBUG ? (process.env.MONGOOSE_DEBUG === 'true') : false,
  },
  developmentLog: process.env.DEVELOPMENT_LOG ? (process.env.DEVELOPMENT_LOG === 'true') : true,
};

export default config;
