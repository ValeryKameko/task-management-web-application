const config = {
  host: process.env.APP_HOST,
  db: {
    username: process.env.MONGO_USERNAME,
    password: process.env.MONGO_PASSWORD,
    hostname: process.env.MONGO_HOSTNAME,
    port: process.env.MONGO_PORT,
    schema: process.env.MONGO_SCHEMA,
    mongoose_debug: false,
  },
  developmentLog: false,
};

export default config;
