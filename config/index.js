/* eslint-disable import/first */
import dotenv from 'dotenv';
import _ from 'lodash';

dotenv.config();

import development from './environment/development';
import production from './environment/production';
import common from './common';


const configs = {
  development: _.merge(common, development),
  production: _.merge(common, production),
};

const config = configs[process.env.NODE_ENV || 'development'];

export default config;
