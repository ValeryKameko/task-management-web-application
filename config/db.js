import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import { initializeServices } from '../src/services';
import config from '.';

mongoose.connect(`mongodb://${config.db.username}:${config.db.password}@${config.db.hostname}:${config.db.port}/${config.db.schema}`, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});
mongoose.set('debug', config.db.mongoose_debug);

autoIncrement.initialize(mongoose.connection);

initializeServices();
