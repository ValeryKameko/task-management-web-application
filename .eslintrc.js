module.exports = {
  "parser": "babel-eslint",
  "env": {
    "es6": true,
    "node": true,
  },
  "extends": [
    "airbnb-base",
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly",
  },
  "parserOptions": {
    "ecmaVersion": 2019,
    "sourceType": "module",
  },
  "rules": {
    "class-methods-use-this": 0,
    "no-underscore-dangle": 0,
  },
};
