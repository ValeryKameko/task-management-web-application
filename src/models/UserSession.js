import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'users' },
  sessionId: String,
});

class UserSessionModel extends Model {}

schema.loadClass(UserSessionModel);

export const UserSession = model('user_sessions', schema);
export default UserSession;
