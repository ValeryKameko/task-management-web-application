import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  userName: {
    type: String,
    unique: true,
  },
  fullName: String,
  password: String,
  sessionIds: [{ type: Schema.Types.ObjectId, ref: 'user_session' }],
  userRoles: [String],
});

class UserModel extends Model {}

schema.loadClass(UserModel);

export const User = model('user', schema);
export default User;
