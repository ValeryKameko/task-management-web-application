import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  value: { type: String, unique: true },
});

schema.index({ value: 1 });

class TaskStatusModel extends Model {}

schema.loadClass(TaskStatusModel);

export const TaskStatus = model('task_statuses', schema);
export default TaskStatus;
