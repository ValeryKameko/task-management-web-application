import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  value: { type: String, unique: true },
});

schema.index({ value: 1 });

class PriorityModel extends Model {}

schema.loadClass(PriorityModel);

export const Priority = model('priorities', schema);
export default Priority;
