export const UserRole = Object.freeze({
  GUEST: 'guest',
  USER: 'user',
});

export default UserRole;
