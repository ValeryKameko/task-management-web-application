import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  value: {
    type: String,
    unique: true,
  },
});

schema.index({ value: 1 });

class TagModel extends Model {}

schema.loadClass(TagModel);

export const Tag = model('tags', schema);
export default Tag;
