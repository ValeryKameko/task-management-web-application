import autoIncrement from 'mongoose-auto-increment';

import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  numberId: Number,
  title: String,
  estimation: Number,
  dueDate: Date,
  description: String,
  tagIds: [{ type: Schema.Types.ObjectId, ref: 'tags' }],
  priorityId: { type: Schema.Types.ObjectId, ref: 'priorities' },
  taskStatusId: { type: Schema.Types.ObjectId, ref: 'task_statuses' },
  attachmentIds: [{ type: Schema.Types.ObjectId, ref: 'attachments' }],
  commentIds: [{ type: Schema.Types.ObjectId, ref: 'comments' }],
  creatorId: { type: Schema.Types.ObjectId, ref: 'users' },
  createdAt: Date,
  updatedAt: Date,
});

schema.plugin(autoIncrement.plugin, { model: 'tasks', field: 'numberId', startAt: 1 });
schema.index({ numberId: 1 });

class TaskModel extends Model {}

schema.loadClass(TaskModel);

export const Task = model('tasks', schema);
export default Task;
