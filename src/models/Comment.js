import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  creatorId: { type: Schema.Types.ObjectId, ref: 'users' },
  text: String,
  createdAt: Date,
  updatedAt: Date,
});

schema.index({ physicalFileName: 1 });

class CommentModel extends Model {}

schema.loadClass(CommentModel);

export const Comment = model('comments', schema);
export default Comment;
