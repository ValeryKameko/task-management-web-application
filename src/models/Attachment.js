import { Schema, Model, model } from 'mongoose';

const schema = new Schema({
  viewFileName: String,
  viewName: String,
  physicalFileName: { type: String, unique: true },
  mimeType: String,
});

schema.index({ physicalFileName: 1 });

class AttachmentModel extends Model {}

schema.loadClass(AttachmentModel);

export const Attachment = model('attachment', schema);
export default Attachment;
