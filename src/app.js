import 'core-js/stable';
import 'regenerator-runtime/runtime';
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from 'body-parser';
import multer from 'multer';
import cookieParser from 'cookie-parser';
import { randomBytes } from 'crypto';

import '../config/db';
import config from '../config';
import { routes, registerRoutes } from './routes';
import { authenticationMiddleware, sessionMiddleware } from './middleware';

export const app = express();

const storage = multer.diskStorage({
  destination: (req, file, next) => {
    next(null, config.files.uploadFolder);
  },
  filename: (req, file, next) => {
    next(null, randomBytes(64).toString('hex'));
  },
});
const upload = multer({ storage });

if (config.developmentLog) app.use(morgan('dev'));
app.use(express.static(`${__dirname}/../public`));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(cookieParser());

app.set('views', './src/views');
app.set('view engine', 'pug');

app.use([sessionMiddleware, authenticationMiddleware]);
app.use(registerRoutes(routes, { upload }));

process.on('SIGINT', () => {
  process.exit();
});

app.listen(config.port, config.host);

export default app;
