import path from 'path';
import moveFile from 'move-file';
import { randomBytes } from 'crypto';
import { promises as fs } from 'fs';

import { Attachment } from '../models/Attachment';
import { ServiceError } from '../errors/ServiceError';
import config from '../../config';

export class AttachmentService {
  static _instance;

  static ATTACHMENT_DIRECTORY = path.join(config.files.resourceFolder, 'attachments');

  static SPLIT_PREFIX = 2;

  async createAttachment({
    file: { originalname, mimetype, ...file }, name,
  }) {
    const physicalFileName = randomBytes(64).toString('hex');
    const physicalFilePath = this.constructAttachmentFilePath(physicalFileName);
    try {
      await fs.mkdir(path.dirname(physicalFilePath), { recursive: true });
      await moveFile(file.path, physicalFilePath);
      const attachment = new Attachment({
        viewFileName: originalname,
        viewName: name,
        mimeType: mimetype,
        physicalFileName,
      });
      return await attachment.save();
    } catch (error) {
      throw new ServiceError(error, 'Unknown attachment creation error');
    }
  }

  async findAttachmentById(attachmentId) {
    if (!attachmentId) return null;
    try {
      return await Attachment.findById(attachmentId).exec();
    } catch (error) {
      throw new ServiceError(error, 'Unknown attachment search error');
    }
  }

  async findAttachmentsByIds(attachmentIds) {
    const attachmentPromises = attachmentIds.map(
      (attachmentId) => this.findAttachmentById(attachmentId),
    );
    return Promise.all(attachmentPromises);
  }

  async deleteAttachmentById(attachmentId) {
    const attachment = await this.findAttachmentById(attachmentId);
    if (!attachment) throw new ServiceError(null, 'Attachment not found');
    const attachmentPath = this.constructAttachmentFilePath(attachment.physicalFileName);
    try {
      await fs.unlink(attachmentPath);
      await Attachment.findByIdAndRemove(attachment._id);
    } catch (error) {
      if (error.code === 'ENOENT') throw new ServiceError(error, 'Attachment not found');
      throw new ServiceError(error, 'Unknown attachment deletion error');
    }
  }

  constructAttachmentFilePath(fileName) {
    const prefix = fileName.slice(0, AttachmentService.SPLIT_PREFIX);
    const suffix = fileName.slice(AttachmentService.SPLIT_PREFIX);
    return path.join(AttachmentService.ATTACHMENT_DIRECTORY, prefix, suffix);
  }

  static get instance() {
    if (!AttachmentService._instance) {
      AttachmentService._instance = new AttachmentService();
    }
    return AttachmentService._instance;
  }
}

export default AttachmentService;
