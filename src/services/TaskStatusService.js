import { TaskStatus } from '../models/TaskStatus';
import { ServiceError } from '../errors/ServiceError';

export class TaskStatusService {
  static _instance;

  static DEFAULT_TASK_STATUSES = ['Pending', 'Work in progress', 'Done', 'Canceled'];

  async findDefaultStatus() {
    return await this.findTaskStatusByName(TaskStatusService.DEFAULT_TASK_STATUSES[0]);
  }

  async findTaskStatusById(taskStatusId) {
    if (!taskStatusId) return null;
    try {
      return await TaskStatus.findById(taskStatusId).exec();
    } catch (error) {
      throw new ServiceError(error, 'Task status search error');
    }
  }

  async findTaskStatusByName(name) {
    if (!name) return null;
    try {
      return await TaskStatus.findOne({ value: name }).exec();
    } catch (error) {
      throw new ServiceError(error, 'Task status search error');
    }
  }

  async findTaskStatusesByIds(taskStatusIds) {
    try {
      const taskStatuses = await TaskStatus.where('_id').in(taskStatusIds).exec();
      while (taskStatuses.length < taskStatusIds.length) taskStatuses.push(null);
      return taskStatuses;
    } catch (error) {
      throw new ServiceError(error, 'Task statuses search error');
    }
  }

  async findAllTaskStatuses() {
    try {
      return await TaskStatus.find().exec();
    } catch (error) {
      throw new ServiceError(error, 'Task status search error');
    }
  }

  async createTaskStatus(name) {
    try {
      const taskStatus = new TaskStatus({ value: name });
      return await taskStatus.save();
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        throw new ServiceError(error, 'Task status already exist');
      }
      throw new ServiceError(error, 'Task status search error');
    }
  }

  async persistTaskStatus(name) {
    try {
      const taskStatus = new TaskStatus({ value: name });
      return await taskStatus.save();
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        return this.findTaskStatusByName(name);
      }
      throw new ServiceError(error, 'Task status search error');
    }
  }

  async initialize() {
    const taskStatusesAsync = TaskStatusService.DEFAULT_TASK_STATUSES.map(
      (taskStatus) => this.persistTaskStatus(taskStatus),
    );
    return Promise.all(taskStatusesAsync);
  }

  static get instance() {
    if (!TaskStatusService._instance) {
      TaskStatusService._instance = new TaskStatusService();
    }
    return TaskStatusService._instance;
  }
}

export default TaskStatusService;
