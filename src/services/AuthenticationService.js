import { genSaltSync, hashSync, compareSync } from 'bcryptjs';

import { SessionService } from './SessionService';
import { UserService } from './UserService';
import { ServiceError } from '../errors/ServiceError';

export class AuthenticationService {
  static _instance = null;

  static COOKIE_KEY = 'Session_ID';

  static COOKIE_DURATION = 1000 * 60 * 60 * 24;

  constructor() {
    this.sessionService = SessionService.instance;
    this.userService = UserService.instance;
  }

  async register(params, userRoles) {
    const salt = genSaltSync(10);
    const password = hashSync(params.password, salt);
    return this.userService.createUser({
      ...params,
      password,
      userRoles,
    });
  }

  async logIn(res, { userName, password }) {
    try {
      const user = await this.userService.findUserByUserName(userName);
      if (!user || !compareSync(password, user.password)) {
        throw new ServiceError(null, 'Username or password is not correct');
      }
      await this.sessionService.createSession(res, user);
    } catch (error) {
      if (error instanceof ServiceError) {
        throw error;
      }
      throw new ServiceError(null, 'Unknown error');
    }
  }

  async logOut(req, res) {
    const { sessionId } = req.session;
    await this.sessionService.deleteSession(res, sessionId);
  }

  static get instance() {
    if (!AuthenticationService._instance) {
      AuthenticationService._instance = new AuthenticationService();
    }
    return AuthenticationService._instance;
  }
}

export default AuthenticationService;
