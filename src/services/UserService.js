import { User } from '../models/User';
import { ServiceError } from '../errors/ServiceError';

export class UserService {
  static _instance;

  async createUser({
    userName, fullName, password, userRoles,
  }) {
    const user = new User({
      userName,
      fullName,
      password,
      userRoles,
    });
    try {
      return await user.save();
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        throw new ServiceError(error, 'User already exists');
      }
      throw new ServiceError(error, 'Unknown user creation error');
    }
  }

  async findUserByUserName(userName) {
    try {
      return await User.findOne({ userName }).exec();
    } catch (error) {
      throw new ServiceError(error, 'Unknown user search error');
    }
  }

  async findUserById(userId) {
    if (!userId) {
      return null;
    }
    try {
      return await User.findById(userId).exec();
    } catch (error) {
      throw new ServiceError(error, 'Unknown user search error');
    }
  }

  static get instance() {
    if (!UserService._instance) {
      UserService._instance = new UserService();
    }
    return UserService._instance;
  }
}

export default UserService;
