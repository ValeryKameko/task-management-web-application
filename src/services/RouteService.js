import routeMap from '../routes/routeMap';

export class RouteService {
  static _instance;

  buildUrl(routeName, params = {}) {
    let link = routeMap[routeName];
    if (!link) return routeName['not-found.get'];
    Object.entries(params).forEach(([key, value]) => {
      const re = new RegExp(`:${key}(?=[?/]|$)`, 'g');
      link = link.replace(re, value);
    });
    return link;
  }

  static get instance() {
    if (!RouteService._instance) {
      RouteService._instance = new RouteService();
    }
    return RouteService._instance;
  }
}

export const routeService = RouteService.instance;
export const urlFor = (url, params = {}) => routeService.buildUrl(url, params);

export default routeService;
