import { Priority } from '../models/Priority';
import { ServiceError } from '../errors/ServiceError';

export class PriorityService {
  static _instance;

  static DEFAULT_PRIORITIES = ['Very low', 'Low', 'Medium', 'High', 'Very high']

  async findPriorityById(priorityId) {
    if (!priorityId) {
      return null;
    }
    try {
      return await Priority.findById(priorityId).exec();
    } catch (error) {
      throw new ServiceError(error, 'Priority search error');
    }
  }

  async findPriorityByName(name) {
    if (!name) {
      return null;
    }
    try {
      return await Priority.findOne({ value: name }).exec();
    } catch (error) {
      throw new ServiceError(error, 'Priority search error');
    }
  }

  async findPrioritiesByIds(priorityIds) {
    try {
      const priorities = await Priority.where('_id').in(priorityIds).exec();
      while (priorities.length < priorityIds.length) priorities.push(null);
      return priorities;
    } catch (error) {
      throw new ServiceError(error, 'Priorities search error');
    }
  }

  async findAllPriorities() {
    try {
      return await Priority.find().exec();
    } catch (error) {
      throw new ServiceError(error, 'Priorities search error');
    }
  }

  async createPriority(name) {
    try {
      const priority = new Priority({ value: name });
      return await priority.save();
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        throw new ServiceError(error, 'Priority already exist');
      }
      throw new ServiceError(error, 'Priorities search error');
    }
  }

  async persistPriority(name) {
    try {
      const priority = new Priority({ value: name });
      return await priority.save();
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        return this.findPriorityByName(name);
      }
      throw new ServiceError(error, 'Priorities search error');
    }
  }

  async initialize() {
    const prioritiesAsync = PriorityService.DEFAULT_PRIORITIES.map(
      (priority) => this.persistPriority(priority),
    );
    return Promise.all(prioritiesAsync);
  }

  static get instance() {
    if (!PriorityService._instance) {
      PriorityService._instance = new PriorityService();
    }
    return PriorityService._instance;
  }
}

export default PriorityService;
