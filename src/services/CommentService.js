import { Comment } from '../models/Comment';
import { UserService } from './UserService';
import { ServiceError } from '../errors/ServiceError';

export class CommentService {
  static _instance;

  constructor() {
    this.userService = UserService.instance;
  }

  async createComment({
    creatorId, text,
  }) {
    const creator = this.userService.findUserById(creatorId);
    if (!creator) throw new ServiceError('Comment creator not found');

    try {
      const comment = new Comment({
        creatorId, text, createdAt: Date.now(),
      });
      return await comment.save();
    } catch (error) {
      throw new ServiceError(error, 'Unknown comment creation error');
    }
  }

  async updateComment({
    commentId, text,
  }) {
    const comment = await this.findCommentById(commentId);
    try {
      comment.text = text;
      comment.updatedAt = Date.now();
      return await comment.save();
    } catch (error) {
      throw new ServiceError(error, 'Unknown comment updation error');
    }
  }

  async findCommentById(commentId) {
    if (!commentId) return null;
    try {
      const comment = await Comment.findById(commentId);
      comment.creator = await this.userService.findUserById(comment.creatorId);
      return comment;
    } catch (error) {
      throw new ServiceError(error, 'Unknown comment search error');
    }
  }

  async findCommentsByIds(commentIds) {
    const commentPromises = commentIds.map(
      (commentId) => this.findCommentById(commentId),
    );
    return Promise.all(commentPromises);
  }

  async deleteCommentById(commentId) {
    const comment = await this.findCommentById(commentId);
    if (!comment) throw new ServiceError(null, 'Comment not found');
    try {
      await Comment.findByIdAndRemove(comment._id);
    } catch (error) {
      throw new ServiceError(error, 'Unknown Comment deletion error');
    }
  }

  static get instance() {
    if (!CommentService._instance) {
      CommentService._instance = new CommentService();
    }
    return CommentService._instance;
  }
}

export default CommentService;
