import moment from 'moment';
import _ from 'lodash';

import { Task } from '../models/Task';
import { ServiceError } from '../errors/ServiceError';
import { TagService } from './TagService';
import { CommentService } from './CommentService';
import { UserService } from './UserService';
import { PriorityService } from './PriorityService';
import { TaskStatusService } from './TaskStatusService';
import { AttachmentService } from './AttachmentService';

export class TaskService {
  static ESTIMATION_REGEX = new RegExp('^\\s*((?<days>\\d+)d)?\\s*'
    + '((?<hours>\\d+)h)?\\s*'
    + '((?<minutes>\\d+)m)?\\s*$');

  static DUE_DATE_FORMAT = 'YYYY-MM-DD';

  static DEFAULT_SEARCH_LIMIT = 10;

  static DEFAULT_PAGE = 1;

  static _instance;

  constructor() {
    this.tagService = TagService.instance;
    this.commentService = CommentService.instance;
    this.priorityService = PriorityService.instance;
    this.attachmentService = AttachmentService.instance;
    this.taskStatusService = TaskStatusService.instance;
    this.userService = UserService.instance;
  }

  async createTask({
    title, description, estimation, tags, dueDate, priority, taskStatus, creatorId,
  }) {
    const dueDateValue = dueDate ? moment(dueDate, TaskService.DUE_DATE_FORMAT).toDate() : null;
    const estimationMinutes = this.calculateEstimation(estimation);
    const tagIds = await this.persistTags(tags);
    const taskStatusId = taskStatus === '' ? null : taskStatus;
    const priorityId = priority === '' ? null : priority;
    if (priorityId && (await this.priorityService.findPriorityById(priorityId)) === null) throw new ServiceError(null, 'Priority is not correct');
    if (taskStatusId && (await this.taskStatusService.findTaskStatusById(taskStatusId)) === null) throw new ServiceError(null, 'Status is not correct');

    const task = new Task({
      title,
      description,
      estimation: estimationMinutes,
      tagIds,
      dueDate: dueDateValue,
      priorityId,
      creatorId,
      taskStatusId,
      createdAt: Date.now(),
    });
    try {
      return await task.save();
    } catch (error) {
      throw new ServiceError(error, 'Unknown task save error');
    }
  }

  async updateTask(task, {
    title, description, estimation, tags, dueDate, priority, taskStatus,
  }) {
    const dueDateValue = dueDate ? moment(dueDate, TaskService.DUE_DATE_FORMAT).toDate() : null;
    const estimationMinutes = this.calculateEstimation(estimation);
    const tagIds = await this.persistTags(tags);
    const taskStatusId = taskStatus === '' ? null : taskStatus;
    const priorityId = priority === '' ? null : priority;
    if (priorityId && (await this.priorityService.findPriorityById(priorityId)) === null) throw new ServiceError(null, 'Priority is not correct');
    if (taskStatusId && (await this.taskStatusService.findTaskStatusById(taskStatusId)) === null) throw new ServiceError(null, 'Status is not correct');

    Object.assign(task, {
      title,
      description,
      estimation: estimationMinutes,
      tagIds,
      dueDate: dueDateValue,
      priorityId,
      taskStatusId,
      updatedAt: Date.now(),
    });
    try {
      return await task.save();
    } catch (error) {
      throw new ServiceError(error, 'Unknown task update error');
    }
  }

  async deleteTaskById(taskId) {
    const task = await this.findTaskById(taskId);
    if (!task) throw new ServiceError(null, 'Task not found');
    try {
      task.commentIds.forEach(async (commentId) => {
        await this.commentService.deleteCommentById(commentId);
      });

      task.attachmentIds.forEach(async (attachmentId) => {
        await this.attachmentService.deleteAttachmentById(attachmentId);
      });

      await Task.findByIdAndRemove(task._id);
    } catch (error) {
      throw new ServiceError(error, 'Unknown Comment deletion error');
    }
  }

  async searchTasks({
    text, page, limit, tags,
    fromDueDate, toDueDate, priority, taskStatus,
  }) {
    const textValue = text || '';
    const fromDueDateValue = this.decodeOptionalDate(fromDueDate);
    const toDueDateValue = this.decodeOptionalDate(toDueDate);
    const pageValue = page || TaskService.DEFAULT_PAGE;
    const limitValue = limit || TaskService.DEFAULT_SEARCH_LIMIT;
    const taskStatusId = taskStatus === '' ? null : taskStatus;
    const priorityId = priority === '' ? null : priority;
    if (priorityId && (await this.priorityService.findPriorityById(priorityId)) === null) throw new ServiceError(null, 'Priority is not correct');
    if (taskStatusId && (await this.taskStatusService.findTaskStatusById(taskStatusId)) === null) throw new ServiceError(null, 'Status is not correct');

    const tagNames = tags ? this.tagService.tagNames(tags) : [];
    const tagIds = await this.tagService.findTagIdsByNames(tagNames);

    let query = Task.find({
      $or: [
        { title: { $regex: _.escapeRegExp(textValue), $options: 'i' } },
        { description: { $regex: _.escapeRegExp(textValue), $options: 'i' } },
      ],
    });
    if (fromDueDateValue) {
      query = query.or([
        { dueDate: { $gte: fromDueDateValue } },
        { dueDate: { $eq: null } }]);
    }
    if (toDueDateValue) {
      query = query.or([
        { dueDate: { $lte: fromDueDateValue } },
        { dueDate: { $eq: null } }]);
    }
    if (tagIds.length > 0) query = query.where('tagIds').all(tagIds);
    if (priorityId) query = query.where('priorityId').eq(priorityId);
    if (taskStatusId) query = query.where('taskStatusId').eq(taskStatusId);
    query = query.skip(limitValue * (pageValue - 1)).limit(limitValue);

    try {
      return await query.exec();
    } catch (error) {
      throw new ServiceError(error, 'Unknown task search error');
    }
  }

  async findTaskById(taskId) {
    if (!taskId) {
      return null;
    }
    try {
      return await Task.findOne({ numberId: taskId }).exec();
    } catch (error) {
      throw new ServiceError(error, 'Unknown task search error');
    }
  }

  async createAttachment(parameters) {
    const { taskId } = parameters;
    if (!taskId) return null;

    let task = await this.findTaskById(taskId);
    if (!task) return null;
    try {
      const attachment = await this.attachmentService.createAttachment(parameters);
      task.attachmentIds.push(attachment._id);
      task = await task.save();
      return await this.loadTaskInfoById(task.numberId);
    } catch (error) {
      throw new ServiceError(error, 'Unknown task attachment creation error');
    }
  }

  async deleteAttachmentById(task, attachmentId) {
    try {
      const newTask = task;
      newTask.attachmentIds = newTask.attachmentIds.filter((id) => !id.equals(attachmentId));
      await task.save();
      await this.attachmentService.deleteAttachmentById(attachmentId);
    } catch (error) {
      throw new ServiceError(error, 'Unknown task attachment deletion error');
    }
  }

  async loadTaskInfoById(taskId) {
    const task = await this.findTaskById(taskId);
    if (!task) return null;

    task.tags = await this.tagService.findTagsByIds(task.tagIds);
    task.formattedTags = task.tags.map((tag) => ({ value: tag.value }));
    task.priority = await this.priorityService.findPriorityById(task.priorityId);
    task.taskStatus = await this.taskStatusService.findTaskStatusById(task.taskStatusId);
    task.attachments = await this.attachmentService.findAttachmentsByIds(task.attachmentIds);
    task.comments = await this.commentService.findCommentsByIds(task.commentIds);
    task.creator = await this.userService.findUserById(task.creatorId);
    task.estimationValue = this.renderEstimation(task.estimation);
    return task;
  }

  static get instance() {
    if (!TaskService._instance) {
      TaskService._instance = new TaskService();
    }
    return TaskService._instance;
  }

  async persistTags(tags) {
    const tagNames = this.tagService.tagNames(tags);
    const tagEntries = await this.tagService.persistTags(tagNames);
    const tagIds = tagEntries.map((tagEntry) => tagEntry._id);
    return tagIds;
  }

  async createComment(parameters) {
    const { taskId } = parameters;
    if (!taskId) return null;

    let task = await this.findTaskById(taskId);
    if (!task) return null;
    try {
      const comment = await this.commentService.createComment({
        ...parameters, text: parameters.comment,
      });
      task.commentIds.push(comment._id);
      task = await task.save();
      return await this.loadTaskInfoById(task.numberId);
    } catch (error) {
      throw new ServiceError(error, 'Unknown task comment creation error');
    }
  }

  async deleteCommentById(task, commentId) {
    try {
      const newTask = task;
      newTask.commentIds = newTask.commentIds.filter((id) => !id.equals(commentId));
      await task.save();
      await this.commentService.deleteCommentById(commentId);
      return this.loadTaskInfoById(task.numberId);
    } catch (error) {
      throw new ServiceError(error, 'Unknown task comment deletion error');
    }
  }

  calculateEstimation(estimationString) {
    const estimationMatch = estimationString.match(TaskService.ESTIMATION_REGEX);
    if (estimationMatch) {
      const { days, hours, minutes } = estimationMatch.groups;
      const estimationMinutes = (days || 0) * 24 * 60 + (hours || 0) * 60 + (minutes || 0);
      return estimationMinutes;
    }
    return null;
  }

  renderEstimation(estimation) {
    const days = Math.floor(estimation / 60 / 24);
    const hours = Math.floor(estimation / 60) % 24;
    const minutes = estimation % 60;

    const estimations = [];
    if (days) estimations.push(`${days}d`);
    if (hours) estimations.push(`${hours}h`);
    if (!(days || hours) || minutes) estimations.push(`${minutes}m`);
    return estimations.join(' ');
  }

  decodeOptionalDate(date) {
    return date ? moment(date, TaskService.DUE_DATE_FORMAT).toDate() : null;
  }
}

export default TaskService;
