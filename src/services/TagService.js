import { Tag } from '../models/Tag';
import { ServiceError } from '../errors/ServiceError';

export class TagService {
  static _instance;

  async persistTag({
    name,
  }) {
    const tag = new Tag({ value: name });
    try {
      return await tag.save();
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        return this.findTagByName(name);
      }
      throw new ServiceError(error, 'Unknown task save error');
    }
  }

  async persistTags(tagNames) {
    try {
      const tagPromises = tagNames.map((tagName) => this.persistTag({ name: tagName }));
      return await Promise.all(tagPromises);
    } catch (error) {
      throw new ServiceError(error, 'Unknown tags persist error');
    }
  }

  async findTagById(tagId) {
    if (!tagId) {
      return null;
    }
    try {
      return await Tag.findById(tagId).exec();
    } catch (error) {
      throw new ServiceError(error, 'Unknown tag search error');
    }
  }

  async findTagIdsByNames(tagNames) {
    const tags = await this.findTagsByNames(tagNames);
    return tags.map((tag) => tag?._id);
  }

  async findTagsByNames(tagNames) {
    try {
      const tags = await Tag.where('value').in(tagNames).exec();
      while (tags.length < tagNames.length) tags.push(null);
      return tags;
    } catch (error) {
      throw new ServiceError(error, 'Tags search error');
    }
  }

  async findTagsByIds(tagIds) {
    try {
      const tags = await Tag.where('_id').in(tagIds).exec();
      while (tags.length < tagIds.length) tags.push(null);
      return tags;
    } catch (error) {
      throw new ServiceError(error, 'Tags search error');
    }
  }

  async findTagByName(tagName) {
    if (!tagName) {
      return null;
    }
    try {
      return await Tag.findOne({ value: tagName }).exec();
    } catch (error) {
      throw new ServiceError(error, 'Tag search error');
    }
  }

  tagNames(tags) {
    if (!tags || tags === '') return [];
    return JSON.parse(tags).map((tag) => tag.value);
  }

  static get instance() {
    if (!TagService._instance) {
      TagService._instance = new TagService();
    }
    return TagService._instance;
  }
}

export default TagService;
