import { PriorityService } from './PriorityService';
import { TaskStatusService} from './TaskStatusService';

export async function initializeServices() {
  await PriorityService.instance.initialize();
  await TaskStatusService.instance.initialize();
}

export default initializeServices;
