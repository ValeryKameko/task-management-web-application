import { randomBytes } from 'crypto';

import { UserService } from './UserService';
import { UserSession } from '../models/UserSession';

export class SessionService {
  static _instance = null;

  static COOKIE_KEY = 'Session_ID';

  static COOKIE_DURATION = 1000 * 60 * 60 * 24;

  constructor() {
    this.userService = UserService.instance;
  }

  async deleteSession(res, sessionId) {
    const session = await UserSession.findOne({ sessionId }).exec();
    const user = await this.userService.findUserById(session.userId);

    user.sessionIds.pull(session._id);
    await user.save();
    await UserSession.deleteOne({ _id: session._id });

    res.clearCookie(SessionService.COOKIE_KEY);
  }

  async createSession(res, user) {
    const sessionId = randomBytes(64).toString('base64');
    const session = await new UserSession({ userId: user._id, sessionId }).save();

    user.sessionIds.push(session);
    await user.save();

    res.cookie(SessionService.COOKIE_KEY, session.sessionId, {
      httpOnly: true,
      maxAge: SessionService.COOKIE_DURATION,
    });
    return session;
  }

  async getSession(req) {
    const sessionId = req.cookies[SessionService.COOKIE_KEY];
    if (!sessionId || (typeof sessionId !== 'string')) {
      return null;
    }
    return UserSession.findOne({ sessionId }).exec();
  }

  static get instance() {
    if (!SessionService._instance) {
      SessionService._instance = new SessionService();
    }
    return SessionService._instance;
  }
}

export default SessionService;
