import { SessionService } from '../services/SessionService';

export async function sessionMiddleware(req, res, next) {
  req.session = await SessionService.instance.getSession(req);
  next();
}

export default sessionMiddleware;
