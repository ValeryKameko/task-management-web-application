import { UserService } from '../services/UserService';
import { UserRole } from '../models/UserRole';

export async function authenticationMiddleware(req, res, next) {
  const { session } = req;
  const user = await UserService.instance.findUserById(session && session.userId);
  req.userRoles = (user && user.userRoles) || [UserRole.GUEST];
  req.currentUser = user;
  next();
}

export default authenticationMiddleware;
