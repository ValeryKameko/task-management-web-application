export class ServiceError extends Error {
  constructor(cause, ...reasons) {
    super(reasons);
    Error.captureStackTrace(this, ServiceError);
    if (cause) {
      console.log(cause);
    }
    this.cause = cause;
    this.reasons = reasons;
  }
}

export default ServiceError;
