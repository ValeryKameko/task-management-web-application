export class AuthenticationValidator {
  static _instance;

  validateRegister({
    userName,
    fullName,
    password,
    confirmPassword,
  }) {
    return this.validateUserName(userName)
      || this.validateFullName(fullName)
      || this.validatePassword(password)
      || this.validateConfirmPassword(confirmPassword, password)
      || {};
  }

  validateLogIn({
    userName,
    password,
  }) {
    return this.validateUserName(userName)
      || this.validatePassword(password)
      || {};
  }

  validateUserName(userName) {
    if (!userName) return { userName: 'Username is empty' };
    return null;
  }

  validateFullName(fullName) {
    if (!fullName) return { fullName: 'Full name is empty' };
    return null;
  }

  validatePassword(password) {
    if (!password) return { password: 'Password is empty' };
    return null;
  }

  validateConfirmPassword(confirmPassword, password) {
    if (!confirmPassword) return { confirmPassword: 'Confirm password' };
    if (confirmPassword !== password) return { confirmPassword: 'Passwords not match' };
    return null;
  }

  get instance() {
    if (!AuthenticationValidator._instance) {
      AuthenticationValidator._instance = new AuthenticationValidator();
    }
    return AuthenticationValidator._instance;
  }
}

export default AuthenticationValidator;
