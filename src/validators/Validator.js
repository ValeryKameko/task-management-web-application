import moment from 'moment';

export class Validator {
  validateDate(fieldName, date, { format, optional = true }) {
    if (!date && optional) return null;
    const momentDate = moment(date, format);
    if (!momentDate.isValid()) {
      return { [fieldName]: 'Incorrect format' };
    }
    return null;
  }

  validateInteger(errorType, number, {
    from = -Infinity, to = Infinity, optional = true, fieldName = null,
  }) {
    if ((number === '' || number === undefined || number === null) && optional) {
      return null;
    }
    const numberValue = Number.parseInt(number, 10);
    if (Number.isSafeInteger(numberValue)) {
      return { [errorType]: `Incorrect number format${fieldName ? ` of ${fieldName}` : ''}` };
    }
    if (numberValue < from) {
      return { [errorType]: `Number ${fieldName ? ` ${fieldName}` : ''} cannot be less than ${from}` };
    }
    if (numberValue > to) {
      return { [errorType]: `Number ${fieldName ? ` ${fieldName}` : ''} cannot be greater than ${to}` };
    }
    return null;
  }

  validatePresent(errorType, value, errorMessage = null) {
    if (!value) return { [errorType]: errorMessage || 'Should not be empty' };
    return null;
  }

  validateEnum(errorType, value, validValues, { optional = true }) {
    if (!value && optional) return null;
    if (validValues.includes(value)) return null;
    return { [errorType]: `Value ${value} is not allowed` };
  }
}

export default Validator;
