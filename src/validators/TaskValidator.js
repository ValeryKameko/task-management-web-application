import { Validator } from './Validator';
import { TaskService } from '../services/TaskService';

export class TaskValidator extends Validator {
  static _instance;

  validateEditTask({
    title, description, tags,
    estimation, dueDate, priority, taskStatus,
  }) {
    return this.validatePresent('title', title)
      || this.validatePresent('description', description)
      || this.validateTags('tags', tags)
      || this.validateEstimation('estimation', estimation)
      || this.validateDate('dueDate', dueDate, { format: TaskService.DUE_DATE_FORMAT })
      || this.validatePriority('priority', priority)
      || this.validateTaskStatus('taskStatus', taskStatus)
      || {};
  }

  validateNewTask({
    title, description, tags,
    estimation, dueDate, priority, taskStatus,
  }) {
    return this.validatePresent('title', title)
      || this.validatePresent('description', description)
      || this.validateTags('tags', tags)
      || this.validateEstimation('estimation', estimation)
      || this.validateDate('dueDate', dueDate, { format: TaskService.DUE_DATE_FORMAT })
      || this.validatePriority('priority', priority)
      || this.validateTaskStatus('taskStatus', taskStatus)
      || {};
  }

  validateSearchTask({
    page, limit, tags,
    fromDueDate, toDueDate, priority, taskStatus,
  }) {
    return this.validateInteger('baseError', page, { from: 1 })
      || this.validateInteger('limit', limit, { from: 1, to: 20 })
      || this.validateDate('fromDueDate', fromDueDate, { format: TaskService.DUE_DATE_FORMAT })
      || this.validateDate('toDueDate', toDueDate, { format: TaskService.DUE_DATE_FORMAT })
      || this.validateTags('tags', tags)
      || this.validatePriority('priority', priority)
      || this.validateTaskStatus('taskStatus', taskStatus)
      || {};
  }

  validateNewAttachment({
    name, file,
  }) {
    return this.validatePresent('baseError', name, 'Attachment name should not be empty')
      || this.validatePresent('baseError', file, 'Attachment file should present')
      || {};
  }

  validateNewComment({
    comment,
  }) {
    return this.validatePresent('comment', comment, 'Comment text should not be empty')
      || {};
  }

  validateTags(errorType, tags, { optional = true } = {}) {
    if (!tags && optional) return null;
    try {
      const tagsJson = JSON.parse(tags);
      const tagValues = tagsJson.map((entry) => entry.value);
      if (!tagValues.every((tagValue) => typeof tagValue === 'string')) {
        return { tags: 'Tags should have string values' };
      }

      const uniqueTagValues = new Set(tagValues);
      if (tagValues.length !== uniqueTagValues.size) {
        return { tags: 'Tags should be unique' };
      }
    } catch (error) {
      return { [errorType]: 'Tags format is incorrect' };
    }
    return null;
  }

  validateEstimation(errorType, estimation) {
    if (!estimation || !estimation.match(TaskService.ESTIMATION_REGEX)) {
      return { [errorType]: 'Incorrect format' };
    }
    return null;
  }

  validateTaskStatus(errorType, taskStatus) {
    if (!taskStatus) return null;
    if (typeof taskStatus !== 'string') return { [errorType]: 'Incorrect format' };
    return null;
  }

  validatePriority(errorType, priority) {
    if (!priority) return null;
    if (typeof priority !== 'string') return { [errorType]: 'Incorrect format' };
    return null;
  }

  static get instance() {
    if (!TaskValidator._instance) {
      TaskValidator._instance = new TaskValidator();
    }
    return TaskValidator._instance;
  }
}

export default TaskValidator;
