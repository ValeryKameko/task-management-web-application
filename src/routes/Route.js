import routeMap from './routeMap';

export default class Route {
  constructor(methods, routeName, handler, config = {}) {
    this.link = routeMap[routeName];
    this.handler = handler;
    this.methods = methods;
    this.config = config;
  }

  register(router, config = {}) {
    this.methods.forEach((method) => this.registerMethod(router, method, config));
  }

  registerMethod(router, method, { upload } = {}) {
    const registerRoute = router[method];
    const handlers = [this.handler];

    const singleUpload = this.config?.upload?.single;
    if (singleUpload) handlers.unshift(upload.single(singleUpload));

    registerRoute.call(router, this.link, ...handlers);
  }
}
