import { Router } from 'express';
import {
  IndexController,
  AuthenticationController,
  AttachmentController,
  TaskController,
} from '../controllers';
import Route from './Route';

export const routes = [
  new Route(['get'], 'index.get', (...args) => IndexController.instance.getIndex(...args)),
  new Route(['get'], 'not-found.get', (...args) => IndexController.instance.getNotFound(...args)),
  new Route(['get'], 'not-authorized.get', (...args) => IndexController.instance.getNotAuthorized(...args)),

  new Route(['get'], 'login.get', (...args) => AuthenticationController.instance.getLogIn(...args)),
  new Route(['post'], 'login.post', (...args) => AuthenticationController.instance.postLogIn(...args)),
  new Route(['get'], 'register.get', (...args) => AuthenticationController.instance.getRegister(...args)),
  new Route(['post'], 'register.post', (...args) => AuthenticationController.instance.postRegister(...args)),
  new Route(['post'], 'logout.post', (...args) => AuthenticationController.instance.postLogOut(...args)),

  new Route(['get'], 'task-new.get', (...args) => TaskController.instance.getNewTask(...args)),
  new Route(['post'], 'task-new.post', (...args) => TaskController.instance.postNewTask(...args)),
  new Route(['get'], 'task-edit.get', (...args) => TaskController.instance.getEditTask(...args)),
  new Route(['post'], 'task-edit.post', (...args) => TaskController.instance.postEditTask(...args)),
  new Route(['post'], 'task-delete.post', (...args) => TaskController.instance.postDeleteTask(...args)),

  new Route(['get'], 'task-view.get', (...args) => TaskController.instance.getTask(...args)),
  new Route(['get'], 'task-search.get', (...args) => TaskController.instance.getSearchTasks(...args)),

  new Route(['post'], 'task-attachment.post', (...args) => TaskController.instance.postAttachmentNew(...args), { upload: { single: 'file' } }),
  new Route(['get'], 'task-attachment.get', (...args) => TaskController.instance.getAttachment(...args)),
  new Route(['post'], 'task-attachment-delete.post', (...args) => TaskController.instance.postAttachmentDelete(...args)),

  new Route(['post'], 'task-comment-new.post', (...args) => TaskController.instance.postCommentNew(...args)),
  new Route(['post'], 'task-comment-delete.post', (...args) => TaskController.instance.postCommentDelete(...args)),
  new Route(['get'], 'task-comment-edit.get', (...args) => TaskController.instance.getCommentEdit(...args)),
  new Route(['post'], 'task-comment-edit.post', (...args) => TaskController.instance.postCommentEdit(...args)),

  new Route(['get'], 'attachment-download.get', (...args) => AttachmentController.instance.getAttachmentDownload(...args)),
];

export function registerRoutes(routeList, config = {}) {
  const router = new Router();

  routeList.forEach((route) => route.register(router, config));

  router.use((req, res) => {
    res.status(404);
    return IndexController.instance.getNotFound(req, res);
  });

  return router;
}
