import { AttachmentService } from '../services/AttachmentService';
import { ServiceError } from '../errors/ServiceError';
import { UserRole } from '../models/UserRole';
import { Controller } from './Controller';
import { urlFor } from '../services/RouteService';

export class AttachmentController extends Controller {
  static _instance;

  constructor() {
    super();
    this.attachmentService = AttachmentService.instance;
  }

  async getAttachmentDownload(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    const attachmentId = req.params.attachmentId || '';

    try {
      const attachment = await this.attachmentService.findAttachmentById(attachmentId);
      const filePath = this.attachmentService.constructAttachmentFilePath(attachment.physicalFileName);
      res.type(attachment.mimeType);
      return res.download(filePath, attachment.viewFileName);
    } catch (error) {
      console.log(error)
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  static get instance() {
    if (!AttachmentController._instance) {
      AttachmentController._instance = new AttachmentController();
    }
    return AttachmentController._instance;
  }
}

export default AttachmentController;
