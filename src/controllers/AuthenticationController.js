import { AuthenticationService } from '../services/AuthenticationService';
import { AuthenticationValidator } from '../validators/AuthenticationValidator';
import { ServiceError } from '../errors/ServiceError';
import { UserRole } from '../models/UserRole';
import { Controller } from './Controller';
import { urlFor } from '../services/RouteService';

export class AuthenticationController extends Controller {
  static _instance;

  constructor() {
    super();
    this.authenticationService = new AuthenticationService();
    this.authenticationValidator = new AuthenticationValidator();
  }

  getLogIn(req, res) {
    if (!req.userRoles.includes(UserRole.GUEST)) {
      return res.redirect(urlFor('index.get'));
    }
    return this.renderLogIn(req, res);
  }

  getRegister(req, res) {
    if (!req.userRoles.includes(UserRole.GUEST)) {
      return res.redirect(urlFor('index.get'));
    }
    return this.renderRegister(req, res);
  }

  async postRegister(req, res) {
    if (!req.userRoles.includes(UserRole.GUEST)) {
      return res.redirect(urlFor('index.get'));
    }
    const errors = this.authenticationValidator.validateRegister(req.body);
    if (Object.entries(errors).length !== 0) {
      return this.renderRegister(req, res, { errors });
    }
    try {
      await this.authenticationService.register(req.body, [UserRole.USER]);
      return this.renderLogIn(req, res);
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderRegister(req, res, { baseErrors: error.reasons });
      }
      return this.renderRegister(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postLogIn(req, res) {
    if (!req.userRoles.includes(UserRole.GUEST)) {
      return res.redirect(urlFor('index.get'));
    }
    const errors = this.authenticationValidator.validateLogIn(req.body);
    if (Object.entries(errors).length !== 0) {
      return this.renderLogIn(req, res, { errors });
    }
    try {
      await this.authenticationService.logIn(res, req.body);
      return res.redirect(urlFor('index.get'));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderLogIn(req, res, { baseErrors: error.reasons });
      }
      return this.renderLogIn(req, res, { baseErrors: ['Unknown error', error] });
    }
  }

  async postLogOut(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return res.redirect(urlFor('index.get'));
    }
    await this.authenticationService.logOut(req, res);
    return res.redirect(urlFor('index.get'));
  }

  renderRegister(req, res, params = {}) {
    return super.renderWithForm('pages/register', req, res, params);
  }

  renderLogIn(req, res, params = {}) {
    return super.renderWithForm('pages/login', req, res, params);
  }

  static get instance() {
    if (!AuthenticationController._instance) {
      AuthenticationController._instance = new AuthenticationController();
    }
    return AuthenticationController._instance;
  }
}

export default AuthenticationController;
