export class UserController {
  static _instance;

  static get instance() {
    if (!UserController._instance) {
      UserController._instance = new UserController();
    }
    return UserController._instance;
  }
}

export default UserController;
