import { Controller } from './Controller';

export class IndexController extends Controller {
  static _instance;

  getIndex(req, res) {
    return this.renderIndex(req, res);
  }

  getNotFound(req, res) {
    return this.renderNotFound(req, res);
  }

  getNotAuthorized(req, res) {
    return this.renderNotAuthorized(req, res);
  }

  renderIndex(req, res, params = {}) {
    return this.render('pages/index', req, res, params);
  }

  static get instance() {
    if (!IndexController._instance) {
      IndexController._instance = new IndexController();
    }
    return IndexController._instance;
  }
}

export default IndexController;
