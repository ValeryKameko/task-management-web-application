import moment from 'moment';

import { urlFor } from '../services/RouteService';

export class Controller {
  renderWithForm(template, req, res, params = {}) {
    return this.render(template, req, res, {
      errors: {},
      baseErrors: [],
      ...params,
    });
  }

  renderNotFound(req, res, params = {}) {
    return this.render('pages/not-found.pug', req, res, params);
  }

  renderNotAuthorized(req, res, params = {}) {
    return this.render('pages/not-authorized.pug', req, res, params);
  }

  renderErrors(req, res, params = {}) {
    return this.render('pages/errors.pug', req, res, params);
  }

  render(template, req, res, params = {}) {
    return res.render(template, {
      ...params,
      currentUser: req.currentUser,
      values: req.body,
      queryValues: req.query,
      urlFor,
      moment,
    });
  }
}

export default Controller;
