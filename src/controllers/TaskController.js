import moment from 'moment';

import { TaskService } from '../services/TaskService';
import { CommentService } from '../services/CommentService';
import { PriorityService } from '../services/PriorityService';
import { TaskStatusService } from '../services/TaskStatusService';
import { TaskValidator } from '../validators/TaskValidator';
import { ServiceError } from '../errors/ServiceError';
import { UserRole } from '../models/UserRole';
import { Controller } from './Controller';
import { urlFor } from '../services/RouteService';

export class TaskController extends Controller {
  constructor() {
    super();
    this.taskService = TaskService.instance;
    this.priorityService = PriorityService.instance;
    this.commentService = CommentService.instance;
    this.taskStatusService = TaskStatusService.instance;
    this.taskValidator = TaskValidator.instance;
  }

  getNewTask(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    return this.renderNewTask(req, res);
  }

  async postNewTask(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    const errors = this.taskValidator.validateNewTask(req.body);
    if (Object.entries(errors).length !== 0) {
      return this.renderNewTask(req, res, { errors });
    }
    try {
      const task = await this.taskService.createTask({
        ...req.body,
        creatorId: req.currentUser._id,
      });
      return res.redirect(urlFor('task-view.get', { id: task.numberId }));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderNewTask(req, res, { baseErrors: error.reasons });
      }
      return this.renderNewTask(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async getTask(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    const taskId = Number.parseInt(req.params.id, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }

    try {
      const task = await this.taskService.loadTaskInfoById(taskId);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }
      return this.renderTask(req, res, { task });
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postEditTask(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }

    const taskId = Number.parseInt(req.params.id, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }
    const task = await this.taskService.loadTaskInfoById(taskId);
    if (!task) return this.renderNotFound(req, res, { entity: 'Task' });

    try {
      const errors = this.taskValidator.validateEditTask(req.body);
      if (Object.entries(errors).length !== 0) {
        return this.renderTask(req, res, { errors, task, edit: true });
      }
      await this.taskService.updateTask(task, req.body);
      return res.redirect(urlFor('task-view.get', { id: task.numberId }));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postDeleteTask(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    console.log('lol');

    const taskId = Number.parseInt(req.params.id, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }
    console.log('lol');
    const task = await this.taskService.loadTaskInfoById(taskId);
    if (!task) return this.renderNotFound(req, res, { entity: 'Task' });
    console.log('kek');
    try {
      await this.taskService.deleteTaskById(taskId);
      return res.redirect(urlFor('task-search.get'));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async getEditTask(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    const taskId = Number.parseInt(req.params.id, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }

    try {
      const task = await this.taskService.loadTaskInfoById(taskId);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }
      return this.renderTask(req, res, { task, edit: true });
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async getSearchTasks(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    const errors = this.taskValidator.validateSearchTask(req.query);
    if (Object.entries(errors).length !== 0) {
      return this.renderSearchTasks(req, res, { errors });
    }
    try {
      let tasks = await this.taskService.searchTasks(req.query);
      tasks = await Promise.all(tasks.map(
        (task) => this.taskService.loadTaskInfoById(task.numberId),
      ));
      return this.renderSearchTasks(req, res, { tasks });
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderSearchTasks(req, res, { baseErrors: error.reasons });
      }
      return this.renderSearchTasks(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postAttachmentNew(req, res) {
    const parameters = { ...req.body, file: req.file };
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }

    const taskId = Number.parseInt(req.params.taskId, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }
    parameters.taskId = taskId;

    try {
      let task = await this.taskService.loadTaskInfoById(taskId);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }

      const errors = this.taskValidator.validateNewAttachment(parameters);
      if (Object.entries(errors).length !== 0) {
        return this.renderTask(req, res, { errors, task });
      }

      task = await this.taskService.createAttachment(parameters);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }
      return res.redirect(urlFor('task-view.get', { id: taskId }));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postAttachmentDelete(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }

    const { attachmentId } = req.params;
    const taskId = Number.parseInt(req.params.taskId, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }

    try {
      const task = await this.taskService.findTaskById(taskId);
      if (!task) return this.renderNotFound(req, res, { entity: 'Task' });
      if (!task.attachmentIds.includes(attachmentId)) return this.renderNotFound(req, res, { entity: 'Attachment' });

      await this.taskService.deleteAttachmentById(task, attachmentId);
      return res.redirect(urlFor('task-view.get', { id: taskId }));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async getCommentEdit(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }
    const taskId = Number.parseInt(req.params.taskId, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }

    try {
      const task = await this.taskService.loadTaskInfoById(taskId);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }
      if (!task.commentIds.indexOf(req.params.commentId)) {
        return this.renderNotFound(req, res, { entity: 'Comment' });
      }
      return this.renderTask(req, res, { task, commentEdit: req.params.commentId });
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postCommentEdit(req, res) {
    const parameters = { ...req.body, commentId: req.params.commentId };
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }

    const taskId = Number.parseInt(req.params.taskId, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }
    parameters.taskId = taskId;

    try {
      const task = await this.taskService.findTaskById(taskId);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }
      if (task.commentIds.indexOf(parameters.commentId) === -1) {
        return this.renderNotFound(req, res, { entity: 'Comment' });
      }
      const comment = await this.commentService.findCommentById(parameters.commentId);
      if (!req.currentUser._id.equals(comment.creatorId)) {
        return this.renderNotAuthorized(req, res);
      }

      await this.commentService.updateComment({ ...parameters, text: parameters.comment });
      return res.redirect(urlFor('task-view.get', { id: taskId }));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postCommentDelete(req, res) {
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }

    const { commentId } = req.params;
    const taskId = Number.parseInt(req.params.taskId, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }

    try {
      const task = await this.taskService.findTaskById(taskId);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }
      if (task.commentIds.indexOf(commentId) === -1) {
        return this.renderNotFound(req, res, { entity: 'Comment' });
      }
      const comment = await this.commentService.findCommentById(commentId);
      if (!req.currentUser._id.equals(comment.creatorId)) {
        return this.renderNotAuthorized(req, res);
      }

      await this.taskService.deleteCommentById(task, req.params.commentId);
      return res.redirect(urlFor('task-view.get', { id: taskId }));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  async postCommentNew(req, res) {
    const parameters = req.body;
    if (!req.userRoles.includes(UserRole.USER)) {
      return this.renderNotAuthorized(req, res);
    }

    const taskId = Number.parseInt(req.params.taskId, 10);
    if (!Number.isSafeInteger(taskId) || taskId < 0) {
      return this.renderNotFound(req, res, { entity: 'Task' });
    }
    parameters.taskId = taskId;
    parameters.creatorId = req.currentUser._id;

    try {
      let task = await this.taskService.loadTaskInfoById(taskId);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }

      const errors = this.taskValidator.validateNewComment(parameters);
      if (Object.entries(errors).length !== 0) {
        return this.renderTask(req, res, { errors, task });
      }

      task = await this.taskService.createComment(parameters);
      if (!task) {
        return this.renderNotFound(req, res, { entity: 'Task' });
      }
      return res.redirect(urlFor('task-view.get', { id: taskId }));
    } catch (error) {
      if (error instanceof ServiceError) {
        return this.renderErrors(req, res, { baseErrors: error.reasons });
      }
      return this.renderErrors(req, res, { baseErrors: ['Unknown error'] });
    }
  }

  renderSearchTasks(req, res, params = {}) {
    return this.render('pages/task-search.pug', req, res, {
      ...params,
    });
  }

  renderNewTask(req, res, params = {}) {
    return this.renderWithForm('pages/task-new.pug', req, res, {
      currentDate: moment().format('YYYY-MM-DD'),
      ...params,
    });
  }

  renderTask(req, res, params = {}) {
    return this.render('pages/task.pug', req, res, params);
  }

  async render(view, req, res, params = {}) {
    const priorities = await this.priorityService.findAllPriorities();
    const taskStatuses = await this.taskStatusService.findAllTaskStatuses();
    return super.render(view, req, res, { ...params, priorities, taskStatuses });
  }

  static get instance() {
    if (!TaskController._instance) {
      TaskController._instance = new TaskController();
    }
    return TaskController._instance;
  }
}

export default TaskController;
