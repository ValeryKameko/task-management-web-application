db = db.getSiblingDB('task_management_application')
db.createUser(
  {
    'user': 'task_management_application_web',
    'pwd': 'task_management_application_password',
    'roles': [
      {
        'role': 'readWrite',
        'db': 'task_management_application',
      },
    ],
  },
);
